#ifndef LOVE_JOYSTICK_NULL_JOYSTICK_MODULE_H
#define LOVE_JOYSTICK_NULL_JOYSTICK_MODULE_H

// LOVE
#include "joystick/JoystickModule.h"
#include "joystick/null/Joystick.h"

namespace love
{
namespace joystick
{
namespace null
{

class JoystickModule : public love::joystick::JoystickModule
{
public:

	JoystickModule() {

	};
	virtual ~JoystickModule() {

	};

	// Implements Module.
	const char *getName() const {
		return "Imaginary module for sticks of joy";
	}

	// Implements JoystickModule.
	love::joystick::Joystick *addJoystick(int deviceindex) {
		return NULL;
	}

	void removeJoystick(love::joystick::Joystick *joystick) {

	};
	love::joystick::Joystick *getJoystickFromID(int instanceid) {
		return NULL;
	};
	love::joystick::Joystick *getJoystick(int joyindex) {
		return NULL;
	}
	int getIndex(const love::joystick::Joystick *joystick) {
		return 0;
	}
	int getJoystickCount() const {
		return 0;
	}

	bool setGamepadMapping(const std::string &guid, Joystick::GamepadInput gpinput, Joystick::JoystickInput joyinput) {
		return false;
	}
	Joystick::JoystickInput getGamepadMapping(const std::string &guid, Joystick::GamepadInput gpinput) {
		Joystick::JoystickInput ji;
		ji.type = Joystick::INPUT_TYPE_AXIS;
		ji.axis = 0;
		ji.button = 0;
		ji.hat.value = Joystick::HAT_INVALID;
		ji.hat.index = 0;
		return ji;
	}
	void loadGamepadMappings(const std::string &mappings) {

	};
	std::string saveGamepadMappings() {
		return "Please don't save this string somewhere, will you?";
	};
};
}
}
}

#endif // LOVE_JOYSTICK_NULL_JOYSTICK_MODULE_H