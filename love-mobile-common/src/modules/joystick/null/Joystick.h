#ifndef LOVE_JOYSTICK_NULL_JOYSICK_H
#define LOVE_JOYSTICK_NULL_JOYSICK_H


// LOVE
#include "joystick/Joystick.h"
#include "common/EnumMap.h"

namespace love
{
namespace joystick
{
namespace null
{

class Joystick : public love::joystick::Joystick 
{
public:
	Joystick(int id) {

	};

	Joystick(int id, int joyindex) {

	};

	virtual ~Joystick() {

	};

	bool open(int deviceindex) {
		return false;
	};

	void close() {

	};

	bool isConnected() const {
		return false;
	};

	const char* getName() const {
		return "Imaginary stick of joy";
	};

	int getAxisCount() const {
		return 0;
	}

	int getButtonCount() const {
		return 0;
	}

	int getHatCount() const {
		return 0;
	}

	float getAxis(int index) const {
		return 0.0f;
	}

	std::vector<float> getAxes() const {
		std::vector<float> flts;
		return flts;
	}

	Hat getHat(int index) const {
		Hat h = HAT_INVALID;
		return h;
	}

	bool isDown(const std::vector<int> &buttonlist) const {
		return false;
	}

	bool openGamepad(int deviceindex) {
		return false;
	}

	bool isGamepad() const {
		return false;
	}

	float getGamepadAxis(GamepadAxis axis) const {
		return 0.0f;
	}

	bool isGamepadDown(const std::vector<GamepadButton> &blist) const {
		return false;
	}

	void *getHandle() const {
		return NULL;
	}

	std::string getGUID() const {
		return "Imaginary stick of joy's GUID";
	}

	int getInstanceID() const {
		return 0;
	}
	int getID() const {
		return 0;
	}

	bool isVibrationSupported() {
		return false;
	}
	bool setVibration(float left, float right, float duration = -1.0f){
		return false;
	}
	bool setVibration(){
		return false;
	}
	void getVibration(float &left, float &right){
		left = 0.0f;
		right = 0.0f;
	}

	static bool getConstant(Hat in, Uint8 &out){
		return false;
	}
	static bool getConstant(Uint8 in, Hat &out){
		return false;
	}

	static bool getConstant(const char *in, GamepadAxis &out){
		return false;
	}
	static bool getConstant(GamepadAxis in, const char *&out){
		return false;
	}

	static bool getConstant(const char *in, GamepadButton &out){
		return false;
	}
	static bool getConstant(GamepadButton in, const char *&out){
		return false;
	}

	static bool getConstant(const char *in, InputType &out){
		return false;
	}
	static bool getConstant(InputType in, const char *&out){
		return false;
	}

};



} // null
} // joystick
} // love

#endif // LOVE_JOYSTICK_NULL_JOYSICK_H